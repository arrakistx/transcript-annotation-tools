# Folding and annotation tools

This repository contains various software tools used to annotate transcripts, particularly with regards to structure predictions.


# Example scripts:

*	**run_rnafold_local.py** takes an Ensembl transcript ID as input and uses RNAfold to generate folding predictions (with tunable local constraints).