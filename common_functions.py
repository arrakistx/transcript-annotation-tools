import sys

def dot_bracket_to_igv_db(db_string, part_string, coord_map, chrom='None', out_file=sys.stdout):
	"""Converts a dot bracket string, with optional partition function string, into a series of bed pairs
	that can be used to visualize RNA structure in IGV.
	
	"""

	if type(out_file) is str:
		out_handle = open(out_file, 'w')
	else:
		out_handle = sys.stdout

	color_dict = {'weak': (192, 192, 192), 'medium': (128, 128, 128), 'strong': (0, 0, 0)}
	color_keys = ['weak', 'medium', 'strong']  # This just establishes 0 = weak, 1 = medium, 2 = strong

	for color in color_keys:
		print('color:', color_dict[color][0], color_dict[color][1], color_dict[color][2], sep='\t', file=out_handle)


	assert len(db_string) == len(part_string)

	left_queue = []
	pairs = []

	for pos, (db_char, part_char) in enumerate(zip(db_string, part_string)):
		if db_char == '(':
			left_queue.append(pos + 1)
		elif db_char == ')':
			left_pos = left_queue.pop()
			right_pos = pos + 1

			pairs.append((left_pos, right_pos, part_string[left_pos - 1], part_string[right_pos - 1]))

	
	for pair in pairs:
		if pair[2] in (',', '.') or pair[3] in (',', '.'):
			color_code = 0
		elif pair[2] in ('|', '{', '}') or pair[3] in ('|', '{', '}'):
			color_code = 1
		else:
			color_code = 2

		print(chrom, coord_map[pair[0]], coord_map[pair[0]],  coord_map[pair[1]], coord_map[pair[1]], color_code,
			  sep='\t', file=out_handle)

	if type(out_file) is str:
		out_handle.close()


def get_cdna_map_from_id(transcript_id):
	""" Uses the Ensembl API to fetch the sequence and genomic mapping for a transcript, given the Ensembl ID."""

	import requests

	cdna_coords_ensembl = 'https://rest.ensembl.org/map/cdna/{}/1..{}?content-type=application/json'
	cdna_seq_ensembl = 'https://rest.ensembl.org/sequence/id/{}?content-type=application/json;type=cdna'

	cdna_seq = requests.get(cdna_seq_ensembl.format(transcript_id)).json()['seq']
	cdna_length = len(cdna_seq)

	cdna_map = requests.get(cdna_coords_ensembl.format(transcript_id, cdna_length)).json()

	coord_mapping = []

	if 'mappings' in cdna_map:

		if cdna_map['mappings'][0]['strand'] == 1:
			ordered_exons = cdna_map['mappings']
		elif cdna_map['mappings'][0]['strand'] == -1:
			ordered_exons = cdna_map['mappings'][::-1]
		else:
			raise Exception('First exon start with a weird strand!')


		for exon_json in ordered_exons:
			chrom = 'chr' + exon_json['seq_region_name']
			for pos in range(exon_json['start'], exon_json['end'] + 1):

				if exon_json['strand'] == 1:
					coord_mapping.append((chrom, pos))
				elif exon_json['strand'] == -1:
					coord_mapping.insert(0, (chrom, pos))

	else:
		print(cdna_map)
		raise Exception('Ensembl API did not return expected "mappings" object. The most likely caused is a bad ID.')

	assert len(coord_mapping) == cdna_length

	return cdna_seq, coord_mapping
