#!/usr/bin/env python3

"""
Runs knotty (see below) and parses the output.

https://github.com/HosnaJabbari/Knotty

Input:
CSV file (e.g., exported from the Registry) that contains "name" and "sequence" columns. Each of these entries will 
be folded.

Output:
TSV file with folding predictions for sub-sequences and stating whether 

"""

from subprocess import Popen, PIPE
import pandas as pd
import argparse
from common_functions import get_pairs_as_set, slide_window

parser = argparse.ArgumentParser(description='Run knotty on a set of transcripts.')

parser.add_argument('-tf', help='Transcript file (.csv)', required=True, metavar='trans_file')
parser.add_argument('-of', help='Output file (.tsv)', required=True, metavar='fold_file')
parser.add_argument('--win', help='Window size', default=100, type=int, metavar='window_size')
parser.add_argument('--step', help='Step size', default=10, type=int, metavar='step_size')
parser.add_argument('--binary_path', help='Location of "knotty" executable. Default: search in $PATH.', default='knotty')

args = parser.parse_args()

## Main script

df = pd.read_csv(args.tf)

assert len(df) == len(df.name.drop_duplicates())

out_handle = open(args.of, 'w')

print('name', 'start_pos', 'end_pos', 'seq', 'dot_bracket', 'energy', 'pk_present', sep='\t', file=out_handle)

for _, row in df.iterrows():

	rna_seq = str(row['sequence'])

	for sub_seq, start_pos, end_pos in slide_window(rna_seq, args.win, args.step):

		print(row['name'], start_pos, end_pos)

		p = Popen([args.binary_path, sub_seq], stdin=PIPE, stdout=PIPE)
		lout, lerr = p.communicate(input=rna_seq.encode())

		out_lines = lout.decode().split('\n')

		try:
			seq = out_lines[0].split(' ')[1]
			dotb, energy = out_lines[1].split()[1:3]

		except:
			print('Could not decode:')
			print(lout.decode())
			continue

		# Determine if structure contains pseudoknots by testing if the pairs implied in the 

		simple_dotb = dotb.replace('[', '(').replace(']', ')')

		if get_pairs_as_set(simple_dotb) == get_pairs_as_set(dotb):
			dotb = simple_dotb
			pk_present = False
		else:
			pk_present = True

		print(row['name'], start_pos + 1, end_pos, seq, dotb, float(energy), pk_present, sep='\t', file=out_handle)

		out_handle.flush()

out_handle.close()