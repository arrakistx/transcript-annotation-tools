#!/usr/bin/env python3

"""
Runs RNALfold and parses the output.

Input:
CSV file exported from the Registry that contains "name" and "sequence" columns

Output:
TSV file containing local stable structures as identified by RNALfold.

"""

from subprocess import Popen, PIPE
import pandas as pd
import argparse

parser = argparse.ArgumentParser(description='Run RNALfold on a set of transcripts.')

parser.add_argument('seq_file', help='Sequence file (.csv)', metavar='seq_file')
parser.add_argument('out_file', help='Output file (.tsv)', metavar='fold_file')
parser.add_argument('--span', help='Controls the "span" parameter in RNALfold. Essentially, the maximum length at which to consider base pairings.', 
					metavar='max_bp_span', type=int, default=150)
parser.add_argument('--z', help="""Maximum z-score to consider. This corrects for probability of finding stable structures as a function of sequence composition. 
					If positive value is provided, will not be used.""", 
					type=float, metavar='z_score', default=-2.0)
parser.add_argument('--binary_path', help='Location of "RNALfold" executable. Default: search in $PATH.', default='RNALfold')

args = parser.parse_args()

df = pd.read_csv(args.seq_file)

assert len(df) == len(df.name.drop_duplicates())


out_handle = open(args.out_file, 'w')

print('name', 'db_string', 'seq', 'ddg', 'start_pos', 'end_pos', 'z_score', sep='\t', file=out_handle)

for _, row in df.iterrows():

	print('Processing', row['name'])

	rna_seq = str(row['sequence'])

	rnalfold_cmd = [args.binary_path, '--span={}'.format(args.span)]

	if args.z < 0:
		rnalfold_cmd.append('-z{}'.format(args.z))

	p = Popen(rnalfold_cmd, stdin=PIPE, stdout=PIPE)
	lout, lerr = p.communicate(input=rna_seq.encode())

	out_lines = lout.decode().split('\n')[::-1][3:]

	for struct in out_lines:

		if '( -' in struct:
			struct = struct.replace('( -', '(-') #  Remove annoying spaces in single digit free energies

		fields = struct.split()

		db_string = fields[0]  # Consider removing unpaired bases at the end. Should investigate why they are there to begin with.

		ddg = float(fields[1][1:-1])

		pos = int(fields[2])
		z_score = float(fields[4])

		seq_len = len(db_string)

		start_pos = pos	
		end_pos = start_pos + seq_len

		seq = rna_seq[start_pos - 1 : end_pos - 1]

		print(row['name'], db_string, seq, ddg, start_pos, end_pos, z_score, sep='\t', file=out_handle)

out_handle.close()