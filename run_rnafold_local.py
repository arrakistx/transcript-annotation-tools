#!/usr/bin/env python3

"""

Runs Vienna RNAfold to generate predictions for a given transcript, given the Ensembl ID as the first argument and the 
base pairing constraint as the second. For more information about the base-pairing constraint, see the documentation
for the --maxBPspan option in RNAfold.

The transcript information (sequence and exon coordinates is obtained through the Ensembl API). This will control
the coordinate system that is returned.

Outputs:

*.seq contains the sequence of the transcript, as retrieved.
*_coord_map.txt contains the mapping between transcript coordinates and genome coordinates (1-based)
*_fold_out_{constraint}.txt contains the RNAfold output (dot-bracket strings, including partition function)
*_folds_{constraint}.bp contains an arc plot that can be visualized in IGV.

"""

from __future__ import print_function
from os.path import join
from subprocess import Popen, PIPE
from common_functions import dot_bracket_to_igv_db, get_cdna_map_from_id
import argparse

parser = argparse.ArgumentParser(description="""Runs RNAfold on Ensembl transcripts and generates dot-bracket strings and arc files.""")

parser.add_argument('transcript_id', help='Ensembl ID for transcript (ENST______)')
parser.add_argument('output_dir', help='Ensembl ID for transcript (ENST______)')
parser.add_argument('--bp_constraint', help='Minimum read coverage to calculate reactivities at a given position', 
                    type=int, default=150)
parser.add_argument('--rnafold_binary', help='Path to RNAfold binary. Default: use binary in $PATH.', default='RNAfold')

args = parser.parse_args()


cdna_seq, coord_mapping = get_cdna_map_from_id(args.transcript_id)
rna_seq = cdna_seq.replace('T', 'U')

if len(rna_seq) > 32000:
	raise Exception('RNAfold has a limit of <32k nt. Consider using script for RNALfold instead.')

# Run RNAfold

fold_file = join(args.output_dir, args.transcript_id + '_fold_out_{}.txt'. format(args.bp_constraint))

out_handle = open(fold_file, 'w')

p = Popen([args.rnafold_binary, '-p', '--maxBPspan={}'.format(args.bp_constraint), '--noPS'], stdin=PIPE, stdout=out_handle)
rnafold_out = p.communicate(input=rna_seq.encode())

out_handle.close()


# Create outputs

coord_map_file = join(args.output_dir, args.transcript_id + '_coord_map.txt')


with open(join(args.output_dir, args.transcript_id + '.seq'), 'w') as out_handle:
	print(cdna_seq, file=out_handle)

with open(coord_map_file, 'w') as out_handle:
	for i, (chrom, genome_pos) in enumerate(coord_mapping):
		print(i + 1, chrom, genome_pos, file=out_handle)


coord_map_dict = {}
for coord_num, (chrom_name, chrom_pos) in enumerate(coord_mapping):
	if coord_num == 0:
		chrom = chrom_name
	coord_map_dict[coord_num + 1] = chrom_pos


# Read fold file to create IGV-compatible .bp file

fold_output = open(fold_file).readlines()

seq = fold_output[0].strip()
mfe_db = fold_output[1][0:len(seq)]
ens_db = fold_output[2][0:len(seq)]

db_file = join(args.output_dir, args.transcript_id + '_folds_{}.bp'. format(args.bp_constraint))

dot_bracket_to_igv_db(mfe_db, ens_db, coord_map_dict, chrom, db_file)

